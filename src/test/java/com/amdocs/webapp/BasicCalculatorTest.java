package com.amdocs.webapp;
import static org.junit.Assert.*;
import org.junit.Test;

public class BasicCalculatorTest {
    @Test
    public void testAdd() throws Exception {

        int k= new BasicCalculator().add(5,5);
        assertEquals("Failed to Add", 10, k);
        
    }

   @Test
    public void testSub() throws Exception {

        int k= new BasicCalculator().sub(8,7);
        assertEquals("Failed to Subtract", 1, k);

    }
}
